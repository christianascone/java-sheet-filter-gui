
# java-sheet-filter-gui
> A java application which reads an excel worksheet and provides a gui for search purpose 

![Version][version-image]
![jdk][java-image]

## Requirements

 - [JDK 8u191](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Development setup

This java project uses Maven as dependency manager.
Dependencies are set in pom.xml.

In order to build a runnable jar you can run the following command:
```sh
mvn package
```


## Usage example

Run the project or the runnable jar, then select the excel file with data and choose the target sheet.

After data loading you can choose filters and search records.



## Release History

* 0.0.4
    * UPDATE: Package name
* 0.0.3
    * FIX: Typos
* 0.0.2
    * FIX: Typo in project title
    * REMOVE: Useless code and unused classes
* 0.0.1
    * First release


## Meta

Christian Ascone – ascone.christian@gmail.com

Distributed under the MIT license. See [``LICENSE``](https://gitlab.com/christianascone/java-sheet-filter-gui/blob/master/LICENSE) for more information.

[https://gitlab.com/christianascone](https://gitlab.com/christianascone)
[https://github.com/christianascone](https://github.com/christianascone)


<!-- Markdown link & img dfn's -->
[version-image]: https://img.shields.io/badge/version-0.0.4-brightgreen.svg
[java-image]: https://img.shields.io/badge/JDK-1.8.0__191-blue.svg
