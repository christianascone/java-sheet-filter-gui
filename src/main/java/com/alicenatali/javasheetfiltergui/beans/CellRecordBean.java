package com.alicenatali.javasheetfiltergui.beans;

import com.alicenatali.javasheetfiltergui.utils.AirlinesExcelReader;
import com.alicenatali.javasheetfiltergui.utils.AIRLINES_EXCEL_COLUMN_ENUM;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import org.apache.poi.ss.usermodel.Cell;

public class CellRecordBean extends RecursiveTreeObject<CellRecordBean> {
    private IntegerProperty year;
    private StringProperty season;
    private StringProperty aircraftManufacturer;
    private StringProperty type;
    private FloatProperty totalAirframeHours;
    private FloatProperty cycles;
    private FloatProperty engines;
    private FloatProperty crewOccupants;
    private FloatProperty passengersOccupants;
    private FloatProperty crewFatalities;
    private FloatProperty passangersFatalities;
    private StringProperty airplaneDamage;
    private StringProperty location;
    private StringProperty phase;
    private StringProperty departure;
    private StringProperty destination;
    private StringProperty airCompany;
    private StringProperty classification;
    private IntegerProperty ranking = new SimpleIntegerProperty(0);

    public ObservableValue<Integer> getYear() {
        return year.asObject();
    }

    public void setYear(IntegerProperty year) {
        this.year = year;
    }

    public StringProperty getSeason() {
        return season;
    }

    public void setSeason(StringProperty season) {
        this.season = season;
    }

    public StringProperty getAircraftManufacturer() {
        return aircraftManufacturer;
    }

    public void setAircraftManufacturer(StringProperty aircraftManufacturer) {
        this.aircraftManufacturer = aircraftManufacturer;
    }

    public StringProperty getType() {
        return type;
    }

    public void setType(StringProperty type) {
        this.type = type;
    }

    public ObservableValue<Float> getTotalAirframeHours() {
        return totalAirframeHours.asObject();
    }

    public void setTotalAirframeHours(FloatProperty totalAirframeHours) {
        this.totalAirframeHours = totalAirframeHours;
    }

    public ObservableValue<Float> getCycles() {
        return cycles.asObject();
    }

    public void setCycles(FloatProperty cycles) {
        this.cycles = cycles;
    }

    public ObservableValue<Float> getEngines() {
        return engines.asObject();
    }

    public void setEngines(FloatProperty engines) {
        this.engines = engines;
    }

    public ObservableValue<Float> getCrewOccupants() {
        return crewOccupants.asObject();
    }

    public void setCrewOccupants(FloatProperty crewOccupants) {
        this.crewOccupants = crewOccupants;
    }

    public ObservableValue<Float> getPassengersOccupants() {
        return passengersOccupants.asObject();
    }

    public void setPassengersOccupants(FloatProperty passengersOccupants) {
        this.passengersOccupants = passengersOccupants;
    }

    public ObservableValue<Float> getCrewFatalities() {
        return crewFatalities.asObject();
    }

    public void setCrewFatalities(FloatProperty crewFatalities) {
        this.crewFatalities = crewFatalities;
    }

    public ObservableValue<Float> getPassangersFatalities() {
        return passangersFatalities.asObject();
    }

    public void setPassangersFatalities(FloatProperty passangersFatalities) {
        this.passangersFatalities = passangersFatalities;
    }

    public StringProperty getAirplaneDamage() {
        return airplaneDamage;
    }

    public void setAirplaneDamage(StringProperty airplaneDamage) {
        this.airplaneDamage = airplaneDamage;
    }

    public StringProperty getLocation() {
        return location;
    }

    public void setLocation(StringProperty location) {
        this.location = location;
    }

    public StringProperty getPhase() {
        return phase;
    }

    public void setPhase(StringProperty phase) {
        this.phase = phase;
    }

    public StringProperty getDeparture() {
        return departure;
    }

    public void setDeparture(StringProperty departure) {
        this.departure = departure;
    }

    public StringProperty getDestination() {
        return destination;
    }

    public void setDestination(StringProperty destination) {
        this.destination = destination;
    }

    public StringProperty getAirCompany() {
        return airCompany;
    }

    public void setAirCompany(StringProperty airCompany) {
        this.airCompany = airCompany;
    }

    public StringProperty getClassification() {
        return classification;
    }

    public void setClassification(StringProperty classification) {
        this.classification = classification;
    }

    public ObservableValue<Integer> getRanking() {
        return ranking.asObject();
    }

    public void setRanking(IntegerProperty ranking) {
        this.ranking = ranking;
    }

    public IntegerProperty getRankingProperty() {
        return ranking;
    }

    public void setValueByCell(Cell cell, AIRLINES_EXCEL_COLUMN_ENUM headerKey) {
        String value = AirlinesExcelReader.getStringValue(cell);
        switch (headerKey) {
            case UNKNOWN:
                break;
            case YEAR:
                this.setYear(new SimpleIntegerProperty((int) Float.parseFloat(value)));
                break;
            case SEASON:
                this.setSeason(new SimpleStringProperty(value));
                break;
            case AIRCRAFT_MANUFACTURER:
                this.setAircraftManufacturer(new SimpleStringProperty(value));
                break;
            case TYPE:
                this.setType(new SimpleStringProperty(value));
                break;
            case TOTAL_AIRFRAME_HOURS:
                this.setTotalAirframeHours(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case CYCLES:
                this.setCycles(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case ENGINES:
                this.setEngines(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case CREW_OCCUPANTS:
                this.setCrewOccupants(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case PASSENGERS_OCCUPANTS:
                this.setPassengersOccupants(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case CREW_FATALITIES:
                this.setCrewFatalities(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case PASSANGERS_FATALITIES:
                this.setPassangersFatalities(new SimpleFloatProperty(Float.parseFloat(value)));
                break;
            case AIRPLANE_DAMAGE:
                this.setAirplaneDamage(new SimpleStringProperty(value));
                break;
            case LOCATION:
                this.setLocation(new SimpleStringProperty(value));
                break;
            case PHASE:
                this.setPhase(new SimpleStringProperty(value));
                break;
            case DEPARTURE:
                this.setDeparture(new SimpleStringProperty(value));
                break;
            case DESTINATION:
                this.setDestination(new SimpleStringProperty(value));
                break;
            case AIR_COMPANY:
                this.setAirCompany(new SimpleStringProperty(value));
                break;
            case CLASSIFICATION:
                this.setClassification(new SimpleStringProperty(value));
                break;
        }
    }
}
