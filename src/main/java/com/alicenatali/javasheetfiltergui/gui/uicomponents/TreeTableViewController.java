package com.alicenatali.javasheetfiltergui.gui.uicomponents;

import com.alicenatali.javasheetfiltergui.beans.CellRecordBean;
import com.alicenatali.javasheetfiltergui.utils.AirlinesExcelReader;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import io.datafx.controller.ViewController;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@ViewController(value = "/fxml/ui/TreeTableView.fxml", title = "Material Design Example")
public class TreeTableViewController {

    private static final String TITLE = "Results: ";
    private static final String PREFIX = "( ";
    private static final String POSTFIX = " )";

    // readonly table view
    @FXML
    private JFXTreeTableView<CellRecordBean> treeTableView;
    @FXML
    private JFXTreeTableColumn<CellRecordBean, Integer> yearColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> seasonColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> aircraftManufacturerColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> typeColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> totalAirframeHoursColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> cyclesColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> enginesColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> crewOccupantsColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> passengersOccupantsColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> crewFatalitiesColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, Float> passangersFatalitiesColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> airplaneDamageColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> locationColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> phaseColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> departureColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> destinationColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> airCompanyColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> classificationColumn;

    @FXML
    private JFXTreeTableColumn<CellRecordBean, String> rankingColumn;


    @FXML
    private JFXTextField searchField;
    @FXML
    private JFXComboBox<String> seasonComboBox;
    @FXML
    private JFXComboBox<String> aircraftManufacturerComboBox;
    @FXML
    private JFXComboBox<Float> enginesComboBox;
    @FXML
    private JFXComboBox<Float> crewOccupantsComboBox;
    @FXML
    private JFXComboBox<Float> passengersOccupantsComboBox;
    @FXML
    private JFXComboBox<Float> crewFatalitiesComboBox;
    @FXML
    private JFXComboBox<Float> passangersFatalitiesComboBox;
    @FXML
    private JFXComboBox<String> locationComboBox;
    @FXML
    private JFXComboBox<String> phaseComboBox;
    @FXML
    private JFXComboBox<String> departureComboBox;
    @FXML
    private JFXComboBox<String> destinationComboBox;
    @FXML
    private JFXComboBox<String> airCompanyComboBox;

    @FXML
    private JFXButton searchButton;
    @FXML
    private JFXButton resetButton;

    @FXML
    private Label treeTableViewCount;
    @FXML
    private JFXTextField searchField2;

    private List<CellRecordBean> excelData;

    /**
     * init fxml when loaded.
     */
    @PostConstruct
    public void init() throws IOException, InvalidFormatException {
        setupReadOnlyTableView();
        setupComboBoxItems();
        setupSearchButton();
        setupResetButton();
        resetFilterPredicate();
    }

    private void setupSearchButton() {
        searchButton.setOnAction(event -> {
            clearDataRanking();
            treeTableView.setPredicate(cellProp -> {
                int ranking = 0;

                final CellRecordBean cell = cellProp.getValue();
                boolean seasonOk;
                if (seasonComboBox.getValue() != null) {
                    seasonOk = cell.getSeason().get().toLowerCase().contains(seasonComboBox.getValue().toLowerCase());
                    if (seasonOk)
                        ranking++;
                }
                boolean aircraftManufacturerOk;
                if (aircraftManufacturerComboBox.getValue() != null) {
                    aircraftManufacturerOk = cell.getAircraftManufacturer().get().toLowerCase().contains(aircraftManufacturerComboBox.getValue().toLowerCase());
                    if (aircraftManufacturerOk)
                        ranking++;
                }
                boolean enginesOk;
                if (enginesComboBox.getValue() != null) {
                    enginesOk = cell.getEngines().getValue().equals(enginesComboBox.getValue());
                    if (enginesOk)
                        ranking++;
                }
                boolean crewOccupantsOk;
                if (crewOccupantsComboBox.getValue() != null) {
                    crewOccupantsOk = cell.getCrewOccupants().getValue().equals(crewOccupantsComboBox.getValue());
                    if (crewOccupantsOk)
                        ranking++;
                }
                boolean passengersOccupantsOk;
                if (passengersOccupantsComboBox.getValue() != null) {
                    passengersOccupantsOk = cell.getPassengersOccupants().getValue().equals(passengersOccupantsComboBox.getValue());
                    if (passengersOccupantsOk)
                        ranking++;
                }
                boolean crewFatalitiesOk;
                if (crewFatalitiesComboBox.getValue() != null) {
                    crewFatalitiesOk = cell.getCrewFatalities().getValue().equals(crewFatalitiesComboBox.getValue());
                    if (crewFatalitiesOk)
                        ranking++;
                }
                boolean passangersFatalitiesOk;
                if (passangersFatalitiesComboBox.getValue() != null) {
                    passangersFatalitiesOk = cell.getPassangersFatalities().getValue().equals(passangersFatalitiesComboBox.getValue());
                    if (passangersFatalitiesOk)
                        ranking++;
                }
                boolean locationOk;
                if (locationComboBox.getValue() != null) {
                    locationOk = cell.getLocation().get().toLowerCase().contains(locationComboBox.getValue().toLowerCase());
                    if (locationOk)
                        ranking++;
                }
                boolean phaseOk;
                if (phaseComboBox.getValue() != null) {
                    phaseOk = cell.getPhase().get().toLowerCase().contains(seasonComboBox.getValue().toLowerCase());
                    if (phaseOk)
                        ranking++;
                }
                boolean departureOk;
                if (departureComboBox.getValue() != null) {
                    departureOk = cell.getDeparture().get().toLowerCase().contains(departureComboBox.getValue().toLowerCase());
                    if (departureOk)
                        ranking++;
                }
                boolean destinationOk;
                if (destinationComboBox.getValue() != null) {
                    destinationOk = cell.getDestination().get().toLowerCase().contains(destinationComboBox.getValue().toLowerCase());
                    if (destinationOk)
                        ranking++;
                }
                boolean airCompanyOk;
                if (airCompanyComboBox.getValue() != null) {
                    airCompanyOk = cell.getAirCompany().get().toLowerCase().contains(airCompanyComboBox.getValue().toLowerCase());
                    if (airCompanyOk)
                        ranking++;
                }
                cell.getRankingProperty().set(ranking);
                return ranking > 0;
            });
        });
    }

    private void setupResetButton() {
        resetButton.setOnAction(event -> {
            clearDataRanking();
            seasonComboBox.valueProperty().set(null);
            aircraftManufacturerComboBox.valueProperty().set(null);
            enginesComboBox.valueProperty().set(null);
            crewOccupantsComboBox.valueProperty().set(null);
            passengersOccupantsComboBox.valueProperty().set(null);
            crewFatalitiesComboBox.valueProperty().set(null);
            passangersFatalitiesComboBox.valueProperty().set(null);
            locationComboBox.valueProperty().set(null);
            phaseComboBox.valueProperty().set(null);
            departureComboBox.valueProperty().set(null);
            destinationComboBox.valueProperty().set(null);
            airCompanyComboBox.valueProperty().set(null);
            resetFilterPredicate();
        });
    }

    private int getFiltersCount() {
        int count = 0;
        if (seasonComboBox.getValue() != null)
            count++;
        if (aircraftManufacturerComboBox.getValue() != null)
            count++;
        if (enginesComboBox.getValue() != null)
            count++;
        if (crewOccupantsComboBox.getValue() != null)
            count++;
        if (passengersOccupantsComboBox.getValue() != null)
            count++;
        if (crewFatalitiesComboBox.getValue() != null)
            count++;
        if (passangersFatalitiesComboBox.getValue() != null)
            count++;
        if (locationComboBox.getValue() != null)
            count++;
        if (phaseComboBox.getValue() != null)
            count++;
        if (departureComboBox.getValue() != null)
            count++;
        if (destinationComboBox.getValue() != null)
            count++;
        if (airCompanyComboBox.getValue() != null)
            count++;
        return count;
    }

    private void resetFilterPredicate() {
        treeTableView.setPredicate(cellProp -> false);
    }

    private void clearDataRanking() {
        excelData.forEach(bean -> bean.getRankingProperty().set(0));
    }

    private void setupComboBoxItems() {
        seasonComboBox.getItems().addAll(excelData.stream().map(b -> b.getSeason().get()).distinct().collect(Collectors.toList()));
        aircraftManufacturerComboBox.getItems().addAll(excelData.stream().map(b -> b.getAircraftManufacturer().get()).distinct().collect(Collectors.toList()));
        enginesComboBox.getItems().addAll(excelData.stream().map(b -> b.getEngines().getValue()).distinct().collect(Collectors.toList()));
        crewOccupantsComboBox.getItems().addAll(excelData.stream().map(b -> b.getCrewOccupants().getValue()).distinct().collect(Collectors.toList()));
        passengersOccupantsComboBox.getItems().addAll(excelData.stream().map(b -> b.getPassengersOccupants().getValue()).distinct().collect(Collectors.toList()));
        crewFatalitiesComboBox.getItems().addAll(excelData.stream().map(b -> b.getCrewFatalities().getValue()).distinct().collect(Collectors.toList()));
        passangersFatalitiesComboBox.getItems().addAll(excelData.stream().map(b -> b.getPassangersFatalities().getValue()).distinct().collect(Collectors.toList()));
        locationComboBox.getItems().addAll(excelData.stream().map(b -> b.getLocation().get()).distinct().collect(Collectors.toList()));
        phaseComboBox.getItems().addAll(excelData.stream().map(b -> b.getPhase().get()).distinct().collect(Collectors.toList()));
        departureComboBox.getItems().addAll(excelData.stream().map(b -> b.getDeparture().get()).distinct().collect(Collectors.toList()));
        destinationComboBox.getItems().addAll(excelData.stream().map(b -> b.getDestination().get()).distinct().collect(Collectors.toList()));
        airCompanyComboBox.getItems().addAll(excelData.stream().map(b -> b.getAirCompany().get()).distinct().collect(Collectors.toList()));
    }

    private <T> void setupCellValueFactory(JFXTreeTableColumn<CellRecordBean, T> column, Function<CellRecordBean, ObservableValue<T>> mapper) {
        column.setCellValueFactory((TreeTableColumn.CellDataFeatures<CellRecordBean, T> param) -> {
            if (column.validateValue(param)) {
                return mapper.apply(param.getValue().getValue());
            } else {
                return column.getComputedValue(param);
            }
        });
    }

    private void setupReadOnlyTableView() throws IOException, InvalidFormatException {
        setupCellValueFactory(yearColumn, CellRecordBean::getYear);
        /*setupCellValueFactory(seasonColumn, CellRecordBean::getSeason);*/
        setupCellValueFactory(aircraftManufacturerColumn, CellRecordBean::getAircraftManufacturer);
        /*setupCellValueFactory(typeColumn, CellRecordBean::getType);
        setupCellValueFactory(totalAirframeHoursColumn, CellRecordBean::getTotalAirframeHours);
        setupCellValueFactory(cyclesColumn, CellRecordBean::getCycles);
        setupCellValueFactory(enginesColumn, CellRecordBean::getEngines);
        setupCellValueFactory(crewOccupantsColumn, CellRecordBean::getCrewOccupants);
        setupCellValueFactory(passengersOccupantsColumn, CellRecordBean::getPassengersOccupants);
        setupCellValueFactory(crewFatalitiesColumn, CellRecordBean::getCrewFatalities);
        setupCellValueFactory(passangersFatalitiesColumn, CellRecordBean::getPassangersFatalities);
        setupCellValueFactory(airplaneDamageColumn, CellRecordBean::getAirplaneDamage);
        setupCellValueFactory(locationColumn, CellRecordBean::getLocation);
        setupCellValueFactory(phaseColumn, CellRecordBean::getPhase);
        setupCellValueFactory(departureColumn, CellRecordBean::getDeparture);
        setupCellValueFactory(destinationColumn, CellRecordBean::getDestination);
        setupCellValueFactory(airCompanyColumn, CellRecordBean::getAirCompany);
        setupCellValueFactory(classificationColumn, CellRecordBean::getClassification);*/
        setupCellValueFactory(rankingColumn, cellRecordBean -> new SimpleStringProperty(String.format("%d/%d", cellRecordBean.getRanking().getValue(), getFiltersCount())));

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        FileChooser.ExtensionFilter exelFilter = new FileChooser.ExtensionFilter("Excels file", "*.xls", "*.xlsx");
        fileChooser.getExtensionFilters().add(exelFilter);
        File selectedFile = fileChooser.showOpenDialog(new Stage());
        AirlinesExcelReader airlinesExcelReader = new AirlinesExcelReader();
        this.excelData = airlinesExcelReader.readExcel(selectedFile.getAbsolutePath());

        ObservableList<CellRecordBean> observableList = toObservableCellRecordBeans(excelData);
        treeTableView.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY);

        treeTableView.setRoot(new RecursiveTreeItem<>(observableList, RecursiveTreeObject::getChildren));

        treeTableView.setShowRoot(false);
        treeTableViewCount.textProperty()
                .bind(Bindings.createStringBinding(() -> {
                            int currentItemsCount = treeTableView.getCurrentItemsCount();
                            return currentItemsCount != 0 ? TITLE + PREFIX + currentItemsCount + POSTFIX : "";
                        },
                        treeTableView.currentItemsCountProperty()));
    }


    private ObservableList<CellRecordBean> toObservableCellRecordBeans(List<CellRecordBean> cellRecordBeans) {
        final ObservableList<CellRecordBean> dummyData = FXCollections.observableArrayList();
        dummyData.addAll(cellRecordBeans);
        return dummyData;
    }

}
