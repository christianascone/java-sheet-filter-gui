package com.alicenatali.javasheetfiltergui.utils;

public enum AIRLINES_EXCEL_COLUMN_ENUM {
    UNKNOWN("UNKNOWN"),
    YEAR("Year"),
    SEASON("Season"),
    AIRCRAFT_MANUFACTURER("Aircraft manufacturer"),
    TYPE("Type"),
    TOTAL_AIRFRAME_HOURS("Total airframe hours"),
    CYCLES("Cycles"),
    ENGINES("# Engines"),
    CREW_OCCUPANTS("Crew occupants"),
    PASSENGERS_OCCUPANTS("Passengers- occupants"),
    CREW_FATALITIES("Crew- fatalities"),
    PASSANGERS_FATALITIES("Passengers- fatalities"),
    AIRPLANE_DAMAGE("Airplane damage"),
    LOCATION("Location"),
    PHASE("Phase"),
    DEPARTURE("Departure"),
    DESTINATION("Destination"),
    AIR_COMPANY("Air company"),
    CLASSIFICATION("Classification");

    private String value;

    AIRLINES_EXCEL_COLUMN_ENUM(String string) {
        this.value = string;
    }

    public String getValue() {
        return value;
    }

    public static AIRLINES_EXCEL_COLUMN_ENUM fromString(String string) {
        for (AIRLINES_EXCEL_COLUMN_ENUM airlines_excel_column_enum : AIRLINES_EXCEL_COLUMN_ENUM.values()) {
            if(airlines_excel_column_enum.getValue().equalsIgnoreCase(string))
                return airlines_excel_column_enum;
        }
        return UNKNOWN;
    }
}
