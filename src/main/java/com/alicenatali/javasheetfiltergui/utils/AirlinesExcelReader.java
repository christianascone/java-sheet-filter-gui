package com.alicenatali.javasheetfiltergui.utils;

import com.alicenatali.javasheetfiltergui.beans.CellRecordBean;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * https://www.callicoder.com/java-read-excel-file-apache-poi/
 */
public class AirlinesExcelReader {
    public static String getStringValue(Cell cell) {
        switch (cell.getCellType()) {
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case STRING:
                return cell.getRichStringCellValue().getString();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                } else {
                    return cell.getNumericCellValue() + "";
                }
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            default:
                return "";
        }
    }

    private Map<AIRLINES_EXCEL_COLUMN_ENUM, Integer> createMap() {
        Map<AIRLINES_EXCEL_COLUMN_ENUM, Integer> map = new HashMap<>();
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.YEAR, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.SEASON, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.AIRCRAFT_MANUFACTURER, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.TYPE, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.TOTAL_AIRFRAME_HOURS, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.CYCLES, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.ENGINES, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.CREW_OCCUPANTS, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.PASSENGERS_OCCUPANTS, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.CREW_FATALITIES, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.PASSANGERS_FATALITIES, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.AIRPLANE_DAMAGE, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.LOCATION, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.PHASE, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.DEPARTURE, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.DESTINATION, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.AIR_COMPANY, null);
        map.put(AIRLINES_EXCEL_COLUMN_ENUM.CLASSIFICATION, null);
        return map;
    }

    /**
     * If {@link Cell} {@link CellType} is STRING, it returns the string value else it returns null
     *
     * @param cell
     * @return
     */
    private static String getStringIfExists(Cell cell) {
        return cell.getCellType() == CellType.STRING ? cell.getStringCellValue() : null;
    }

    public List<CellRecordBean> readExcel(String excelFilePath) throws IOException, InvalidFormatException {
        List<CellRecordBean> cellRecordBeans = new LinkedList<>();
        Map<AIRLINES_EXCEL_COLUMN_ENUM, Integer> headers = createMap();

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(excelFilePath));

        // 3. Or you can use a Java 8 forEach with lambda
        System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
        List<String> choices = new LinkedList<>();
        workbook.forEach(sheet -> {
            System.out.println("=> " + sheet.getSheetName());
            choices.add(sheet.getSheetName());
        });

        ChoiceDialog<String> dialog = new ChoiceDialog<>(null, choices);
        dialog.setTitle("Sheet Choice");
        dialog.setHeaderText("Choose the excel sheet with data");
        dialog.setContentText("Choose your sheet:");
        dialog.getDialogPane().lookupButton(ButtonType.CANCEL).setVisible(false);
        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        int choosenSheetIndex = 0;
        if (result.isPresent()) {
            for (int i = 0; i < choices.size(); i++) {
                if (result.get().equalsIgnoreCase(choices.get(i))) {
                    choosenSheetIndex = i;
                    break;
                }
            }
        } else {
            System.exit(0);
        }
        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
        */

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(choosenSheetIndex);

        sheet.forEach(row -> {
            CellRecordBean cellRecordBean = new CellRecordBean();
            row.forEach(cell -> {
                String data = getStringIfExists(cell);
                AIRLINES_EXCEL_COLUMN_ENUM headerKey = AIRLINES_EXCEL_COLUMN_ENUM.fromString(data);
                Integer headerIndex = headers.get(headerKey);
                if (headerKey != AIRLINES_EXCEL_COLUMN_ENUM.UNKNOWN && headerIndex == null) {
                    headers.put(headerKey, cell.getColumnIndex());
                } else {
                    headers.forEach((key, value) -> {
                        if (value != null && value.equals(cell.getColumnIndex()))
                            cellRecordBean.setValueByCell(cell, key);
                    });
                }
            });
            if (cellRecordBean.getSeason() != null)
                cellRecordBeans.add(cellRecordBean);
        });

        // Closing the workbook
        workbook.close();


        return cellRecordBeans;
    }
}
